
import 'package:flutter_test/flutter_test.dart';

void assertFilesIdentical(Map<dynamic, dynamic> originalData, Map<dynamic, dynamic> newData) {

	if (originalData.length != newData.length) {

		final List<int> keysInOriginalDataButNotNewData = <int>[];
		for (final int key in originalData.keys) {
			if (!newData.containsKey(key)) {
				keysInOriginalDataButNotNewData.add(key);
			}
		}

		final List<int> keysInNewDataButNotOldData = <int>[];
		for (final int key in newData.keys) {
			if (!originalData.containsKey(key)) {
				keysInNewDataButNotOldData.add(key);
			}
		}

		fail("Original data length is ${originalData.length} (+$keysInOriginalDataButNotNewData) while new data length is ${newData.length} (+$keysInNewDataButNotOldData)");
	}

	for (final MapEntry<dynamic, dynamic> comparisonEntry in newData.entries) {

		final dynamic dataEntry = originalData[comparisonEntry.key];

		if (dataEntry == null) {
			fail("Key $comparisonEntry not found in data ");
		}

		if (comparisonEntry.value is int || comparisonEntry.value is double || comparisonEntry.value is bool) {
			if (comparisonEntry.value != dataEntry) {
				fail("Value at ${comparisonEntry.key} does not match. Should be ${comparisonEntry.value}, but got $dataEntry");
			}
		} else if (comparisonEntry.value is List<dynamic>) {
			if (comparisonEntry.value.length != dataEntry.length) {
				fail("Value at ${comparisonEntry.key} has mismatched length. Should be ${comparisonEntry.value.length}, but got ${dataEntry.length}");
			}
			for (int i = 0; i < comparisonEntry.value.length; i++) {
				expect(comparisonEntry.value[i], dataEntry[i]);
			}
		} else if (comparisonEntry.value is Map<int, double>) {
			if (comparisonEntry.value.length != dataEntry.length) {
				fail("Value at ${comparisonEntry.key} has mismatched length. Should be ${comparisonEntry.value.length}, but got ${dataEntry.length}");
			}
			for (final MapEntry<int, double> value in comparisonEntry.value.entries) {
				if (value.value != dataEntry[value.key]) {
					fail("Value at ${comparisonEntry.key} at key ${value.key} does not match. Should be ${value.value}, but got ${dataEntry[value.key]}");
				}
			}
		} else if (comparisonEntry.value is List<Map<int, dynamic>>) {
			if (comparisonEntry.value.length != dataEntry.length) {
				fail("Value at ${comparisonEntry.key} has mismatched length. Should be ${comparisonEntry.value.length}, but got ${dataEntry.length}");
			}
			for (int i = 0; i < comparisonEntry.value.length; i++) {
				if (comparisonEntry.value[i].length != dataEntry[i].length) {
					fail("Value at ${comparisonEntry.key} has mismatched length. Should be ${comparisonEntry.value.length}, but got ${dataEntry.length}");
				}
				for (final MapEntry<int, dynamic> value in comparisonEntry.value[i].entries) {
					expect(value.value, dataEntry[i][value.key]);
				}
			}
		} else if (comparisonEntry.value is Map<dynamic, dynamic>) {
			assertFilesIdentical(comparisonEntry.value, dataEntry);
		} else {
			fail("Unknown type: ${comparisonEntry.value.runtimeType} Key: $comparisonEntry");
		}
	}

	for (final MapEntry<dynamic, dynamic> comparisonEntry in originalData.entries) {

		final dynamic dataEntry = newData[comparisonEntry.key];

		if (dataEntry == null) {
			fail("Key $comparisonEntry not found in data ");
		}
	}
}

void expectNonZero(num value, {String? notes}) {
	if (value == 0) {
		fail(notes ?? "");
	}
}

void expectNotNull(dynamic value, {String? notes}) {
	if (value == null) {
		fail(notes ?? "");
	}
}

void expectNotEqual<T>(T item, T comparison, {String? notes}) {
	if (item == comparison) {
		fail(notes ?? "");
	}
}

void expectGreaterThan(num largerItem, num smallerItem, {String? notes}) {
	if (largerItem <= smallerItem) {
		if (notes == null) {
			fail("$largerItem is not greater than $smallerItem");
		} else {
			fail("$largerItem is not greater than $smallerItem Notes: $notes");
		}
	}
}

void expectGreaterThanOrEqual(num largerItem, num smallerItem, {String? notes}) {
	if (largerItem < smallerItem) {
		if (notes == null) {
			fail("$largerItem is not greater than $smallerItem");
		} else {
			fail("$largerItem is not greater than $smallerItem Notes: $notes");
		}
	}
}

void expectInRange(num value, num min, num max, {String? notes}) {
	if (value < min || value > max) {
		fail("Value $value is not in range ($min, $max) ${notes ?? ""}");
	}
}

void expectWithErrorRange(num value, num expectedValue, num error, {String? notes}) {
	expectInRange(value, expectedValue - error, expectedValue + error, notes: notes);
}

void expectSimilar(int value, int expected, int maxDelta, {String? notes}) {
	if ((value - expected).abs() > maxDelta) {
		if (notes == null) {
			fail("$value is not in range [${expected - maxDelta}, ${expected + maxDelta}]");
		} else {
			fail("$value is not in range [${expected - maxDelta}, ${expected + maxDelta}] $notes");
		}
	}
}

void expectApproxEqual(num actual, num matcher, {String? reason, num delta = 0.00001}) {
	expect(actual > matcher - delta, true, reason: "Expected: (> ${matcher - delta}), Actual: $actual"
			"${reason != null ? "\n$reason" : ""}");
	expect(actual < matcher + delta, true, reason: "Expected: (< ${matcher + delta}), Actual: $actual"
			"${reason != null ? "\n$reason" : ""}");
}

void expectNonEmpty(String string, {String? notes}) {
	if (string == "") {
		fail(notes ?? "");
	}
}
