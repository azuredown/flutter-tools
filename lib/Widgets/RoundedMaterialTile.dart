
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// From https://github.com/flutter/flutter/issues/12583 (not in a package for
// whatever reason)
import 'CustomRoundedRectangle.dart';

const double _CORNER_ROUNDNESS = 30;
const double _PADDING_VALUE = 4;

class RoundedMaterialTile extends StatefulWidget {

	const RoundedMaterialTile({this.child, this.first = false, this.last = false, required this.cardColour, required this.borderColour, super.key});

	final Widget? child;
	final bool first, last;
	final Color cardColour, borderColour;

	@override
	RoundedMaterialTileState createState() => RoundedMaterialTileState();
}
class RoundedMaterialTileState extends State<RoundedMaterialTile> {

	@override
	Widget build(BuildContext context) {

		CustomRoundedRectangleBorder border;
		EdgeInsets? padding;

		if (widget.first && widget.last) {
			border = CustomRoundedRectangleBorder(
				borderRadius: const BorderRadius.all(Radius.circular(20.0)),
				topLeftCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				topSide: BorderSide(width: 0.5, color: widget.borderColour),
				topRightCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				bottomLeftCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				bottomSide: BorderSide(width: 0.5, color: widget.borderColour),
				bottomRightCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				leftSide: BorderSide(width: 0.5, color: widget.borderColour),
				rightSide: BorderSide(width: 0.5, color: widget.borderColour),
			);
			padding = const EdgeInsets.symmetric(vertical: _PADDING_VALUE);
		} else if (widget.first) {
			border = CustomRoundedRectangleBorder(
				borderRadius: const BorderRadius.vertical(top: Radius.circular(_CORNER_ROUNDNESS)),
				bottomLeftCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomRightCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				topLeftCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				topSide: BorderSide(width: 0.5, color: widget.borderColour),
				topRightCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				leftSide: BorderSide(width: 0.5, color: widget.borderColour),
				rightSide: BorderSide(width: 0.5, color: widget.borderColour),
			);
			padding = const EdgeInsets.only(top: _PADDING_VALUE);
		} else if (widget.last) {
			border = CustomRoundedRectangleBorder(
				borderRadius: const BorderRadius.vertical(bottom: Radius.circular(_CORNER_ROUNDNESS)),
				topLeftCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				topSide: BorderSide(width: 0.5, color: widget.cardColour),
				topRightCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomLeftCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				bottomSide: BorderSide(width: 0.5, color: widget.borderColour),
				bottomRightCornerSide: BorderSide(width: 0.5, color: widget.borderColour),
				leftSide: BorderSide(width: 0.5, color: widget.borderColour),
				rightSide: BorderSide(width: 0.5, color: widget.borderColour),
			);
			padding = const EdgeInsets.only(bottom: _PADDING_VALUE);
		} else {
			border = CustomRoundedRectangleBorder(
				topLeftCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				topSide: BorderSide(width: 0.5, color: widget.cardColour),
				topRightCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomLeftCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomSide: BorderSide(width: 0.5, color: widget.cardColour),
				bottomRightCornerSide: BorderSide(width: 0.5, color: widget.cardColour),
				leftSide: BorderSide(width: 0.5, color: widget.borderColour),
				rightSide: BorderSide(width: 0.5, color: widget.borderColour),
			);
		}

		// Neither first nor last card
		if (padding == null) {
			return Material(
				elevation: widget.last ? 2 : 0,
				shape: border,
				color: widget.cardColour,
				child: widget.child,
			);
		}

		return Material(
			elevation: widget.last ? 2 : 0,
			shape: border,
			color: widget.cardColour,
			child: Padding(
				padding: padding,
				child: widget.child,
			)
		);
	}
}
