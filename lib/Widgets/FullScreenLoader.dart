
import 'package:custom_tools/BasicExtensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FullScreenLoader extends StatelessWidget {
	const FullScreenLoader({required this.child, required this.loading, super.key});

	final Widget child;
	final bool loading;

	@override
	Widget build(BuildContext context) {
		return Stack(
			children: <Widget>[
				child,
				loading ? SizedBox.expand(
					child: ColoredBox(
						color: Colors.black54,
						child: Center(
							child: DecoratedBox(
								decoration: const BoxDecoration(
									color: Colors.black54,
									borderRadius: BorderRadius.all(Radius.circular(10)),
								),
								child: Padding(
									padding: const EdgeInsets.all(30),
									child: CircularProgressIndicator(color: Theme.of(context).darkModeEnabled ? null : Theme.of(context).primaryColorLight),
								),
							),
						),
					),
				) : const SizedBox.shrink(),
			],
		);
	}
}
