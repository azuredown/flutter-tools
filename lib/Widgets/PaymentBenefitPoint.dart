
import 'package:custom_tools/BasicFunctions.dart';
import 'package:flutter/material.dart';

class PaymentBenefitPoint extends StatelessWidget {
	const PaymentBenefitPoint({
		required this.icon,
		required this.benefitName,
		required this.benefitDescription,
		this.compact = true,
		this.displayProLabel = false,
		super.key,
	});

	final bool compact;
	final IconData icon;
	final String benefitName, benefitDescription;
	final bool displayProLabel;

	@override
	Widget build(BuildContext context) {
		return Row(
			crossAxisAlignment: CrossAxisAlignment.start,
			children: <Widget>[
				Flexible(
					flex: compact ? 2 : 3,
					fit: FlexFit.tight,
					child: Align(
						alignment: Alignment.topRight,
						child: Padding(
							padding: const EdgeInsets.only(right: 10),
							child: Icon(
								icon,
								size: 30,
							),
						),
					),
				),
				Flexible(
					flex: compact ? 4 : 8,
					fit: FlexFit.tight,
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.start,
						children: <Widget>[
							ProLabel(
								displayProLabel: displayProLabel,
								child: Text(benefitName, style: Theme.of(context).textTheme.bodyLarge?.copyWith(fontWeight: FontWeight.bold)),
							),
							Text(benefitDescription),
							const SizedBox(height: 20),
						],
					),
				),
				Flexible(
					flex: compact ? 1 : 2,
					fit: FlexFit.tight,
					child: const SizedBox.shrink(),
				),
			],
		);
	}
}

class ProLabel extends StatelessWidget {
	const ProLabel({
		this.displayProLabel = true,
		required this.child,
		super.key,
	});

	final bool displayProLabel;
	final Widget child;

	@override
	Widget build(BuildContext context) {
		if (!displayProLabel) {
			return child;
		}
		return Row(
			children: <Widget>[
				Flexible(
					child: child,
				),
				const SizedBox(width: 8),
				DecoratedBox(
					decoration: BoxDecoration(
						border: Border.all(
							color: Theme.of(context).disabledColor,
						),
						borderRadius: const BorderRadius.all(Radius.circular(5)),
					),
					child: Center(
						child: Padding(
							padding: EdgeInsets.only(left: 5, bottom: isIOS() ? 4 : 2.5, right: 4.5, top: isIOS() ? 1.9 : 3),
							child: const Text("Pro", style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold)),
						),
					),
				),
			],
		);
	}
}
