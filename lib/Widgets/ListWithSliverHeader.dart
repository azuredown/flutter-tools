
import 'dart:math';

import 'package:custom_tools/BasicExtensions.dart';
import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/Colours.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ListWithSliverHeader extends StatefulWidget {

	const ListWithSliverHeader({
		this.controller,
		this.headerWidget,
		this.headerText,
		this.overlayStyle,
		this.body,
		this.leftButtons,
		this.rightButtons,
		this.scrollPhysics,
		this.expandedAreaHeightOffset,
		this.headerPadding = const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 12),
		this.automaticallyImplyLeading = true,
		this.collapsedElevation = 4,
		this.expandedElevation = 0,
		this.forceElevated = false,
		this.collapsedBackgroundColour,
		this.expandedBackgroundColour,
		super.key,
	}) : assert(headerWidget != null || headerText != null, "Header widget and header text should not both be given"),
				assert(headerWidget == null || headerText == null, "Header widget and header text should not both be null");

	final ScrollPhysics? scrollPhysics;
	final double? expandedAreaHeightOffset;
	final Widget? headerWidget;
	final String? headerText;
	final Widget? body;
	final Widget? rightButtons;
	final Widget? leftButtons;
	final SystemUiOverlayStyle? overlayStyle;
	final Color? expandedBackgroundColour;
	final Color? collapsedBackgroundColour;
	final ScrollController? controller;
	final EdgeInsets headerPadding;
	final bool automaticallyImplyLeading;
	final double collapsedElevation;
	final double expandedElevation;
	final bool forceElevated;

	@override
	ListWithSliverHeaderState createState() => ListWithSliverHeaderState();
}

const double sliverHeaderBottomPadding = 5;

class ListWithSliverHeaderState extends State<ListWithSliverHeader> {

	@override
	Widget build(BuildContext context) {
		return ListWithSliverHeaderRaw(
			controller: widget.controller,
			overlayStyle: widget.overlayStyle,
			body: widget.body,
			leftButtons: widget.leftButtons,
			rightButtons: widget.rightButtons,
			scrollPhysics: widget.scrollPhysics,
			expandedAreaHeightOffset: widget.expandedAreaHeightOffset,
			automaticallyImplyLeading: widget.automaticallyImplyLeading,
			collapsedElevation: widget.collapsedElevation,
			expandedElevation: widget.expandedElevation,
			forceElevated: widget.forceElevated,
			collapsedBackgroundColour: widget.collapsedBackgroundColour,
			expandedBackgroundColour: widget.expandedBackgroundColour,
			makeHeader: (_, __) => FittedBox(
				fit: BoxFit.scaleDown,
				child: Padding(
					padding: widget.headerPadding,
					child: widget.headerText != null
							? Text(widget.headerText!, style: Theme.of(context).textTheme.displaySmall!.copyWith(color: primaryTextColour(Theme.of(context).darkModeEnabled)))
							: widget.headerWidget,
				),
			),
		);
	}
}

const Radius DEFAULT_BORDER_RADIUS = Radius.circular(20);

// Sliver padding is used for things without their own padding like text
const double SLIVER_PADDING = 10;

/// Elevation should be in the range [0, 1]
Color getCardColour(ThemeData themeData, double elevation) {
	if (themeData.darkModeEnabled) {
		return ElevationOverlay.applySurfaceTint(themeData.cardColor, themeData.colorScheme.surfaceTint, pow(1.5 * elevation, 5).toDouble());
	} else {
		return ElevationOverlay.applySurfaceTint(themeData.cardColor, themeData.colorScheme.surfaceTint, 1 - elevation);
	}
}

class CardBackground extends StatelessWidget {

	const CardBackground({required this.child, super.key});

	final Widget child;

	@override
	Widget build(BuildContext context) {
		return DecoratedBox(
			decoration: BoxDecoration(
				color: getCardColour(Theme.of(context), 0.6),
				boxShadow: const <BoxShadow>[
					BoxShadow(
						offset: Offset(0, 2),
						blurRadius: 2,
						color: Colors.black26,
					),
				],
				borderRadius: const BorderRadius.all(DEFAULT_BORDER_RADIUS),
			),
			child: child,
		);
	}
}

class ButtonGroup extends StatelessWidget {
	const ButtonGroup({
		required this.children,
		this.title,
		this.bottomText,
		super.key,
	});

	final String? title, bottomText;
	final List<Widget> children;

	@override
	Widget build(BuildContext context) {
		return Padding(
			padding: const EdgeInsets.symmetric(horizontal: 20),
			child: Column(
				children: <Widget>[
					title != null ? Padding(
						padding: const EdgeInsets.only(
							left: 15,
							bottom: 5,
						),
						child: SizedBox(
							width: double.infinity,
							child: Text(title!),
						),
					) : const SizedBox.shrink(),
					CardBackground(
						child: Padding(
							padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
							child: SizedBox(
								width: double.infinity,
								child: Column(
									children: children,
								),
							),
						),
					),
					bottomText != null ?  Padding(
						padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 35),
						child: Text(bottomText!),
					) : const SizedBox.shrink(),
				],
			),
		);
	}
}

class ListWithSliverHeaderRaw extends StatefulWidget {

	const ListWithSliverHeaderRaw({
		this.controller,
		required this.makeHeader,
		this.overlayStyle,
		this.body,
		this.leftButtons,
		this.rightButtons,
		this.scrollPhysics,
		this.expandedAreaHeightOffset,
		this.automaticallyImplyLeading = true,
		this.collapsedElevation = 4,
		this.expandedElevation = 0,
		this.forceElevated = false,
		this.collapsedBackgroundColour,
		this.expandedBackgroundColour,
		this.extraHeight = 0,
		super.key,
	});

	final ScrollPhysics? scrollPhysics;
	final double? expandedAreaHeightOffset;
	final Widget Function(double percentOfMaxHeight, BoxConstraints constraints) makeHeader;
	final Widget? body;
	final Widget? rightButtons;
	final Widget? leftButtons;
	final SystemUiOverlayStyle? overlayStyle;
	final Color? expandedBackgroundColour;
	final Color? collapsedBackgroundColour;
	final ScrollController? controller;
	final bool automaticallyImplyLeading;
	final double collapsedElevation;
	final double expandedElevation;
	final bool forceElevated;
	final double extraHeight;

	@override
	ListWithSliverHeaderRawState createState() => ListWithSliverHeaderRawState();
}

class ListWithSliverHeaderRawState extends State<ListWithSliverHeaderRaw> {

	@override
	Widget build(BuildContext context) {

		final double safeAreaHeight = MediaQuery.of(context).padding.top;
		final double expandedHeight = widget.extraHeight + min(MediaQuery.of(context).size.height * 0.5, MediaQuery.of(context).size.width * 0.9) + (widget.expandedAreaHeightOffset ?? 0) - sliverHeaderBottomPadding;
		final double minHeight = kToolbarHeight + widget.extraHeight;

		final List<Widget> slivers = <Widget>[
			SliverAppBar(
				forceElevated: widget.forceElevated,
				automaticallyImplyLeading: widget.automaticallyImplyLeading,
				systemOverlayStyle: widget.overlayStyle,
				scrolledUnderElevation: widget.collapsedElevation,
				excludeHeaderSemantics: true,
				pinned: true,
				expandedHeight: expandedHeight,
				toolbarHeight: minHeight,
				iconTheme: IconThemeData(
					color: primaryTextColour(Theme.of(context).darkModeEnabled),
				),
				flexibleSpace: LayoutBuilder(
					builder: (BuildContext context, BoxConstraints constraints) {

						// This without the min height can go up to 1.11 o.o
						final double percentOfMaxHeight = max(min((constraints.maxHeight - minHeight) / (expandedHeight - minHeight), 1), 0);

						final Color? colour = Color.lerp(
								widget.collapsedBackgroundColour ?? Theme.of(context).canvasColor,
								widget.expandedBackgroundColour ?? Theme.of(context).scaffoldBackgroundColor,
								min(1, percentOfMaxHeight * 2));

						return Material(
							elevation: lerp(widget.collapsedElevation, widget.expandedElevation, min(1, percentOfMaxHeight * 2)),
							color: colour,
							child: SizedBox(
								child: Stack(
									children: <Widget>[
										Padding(
											padding: EdgeInsets.only(top: safeAreaHeight),
											child: Center(
												child: SizedBox(
													height: lerp(minHeight, constraints.maxHeight, pow(percentOfMaxHeight, 2).toDouble()),
													width: lerp(constraints.maxWidth - 120, constraints.maxWidth, pow(percentOfMaxHeight, 2).toDouble()),
													child: widget.makeHeader(percentOfMaxHeight, constraints),
												),
											),
										),
										widget.rightButtons == null ? const SizedBox() : Align(
											alignment: Alignment.bottomRight,
											child: widget.rightButtons,
										),
										widget.leftButtons == null ? const SizedBox() : Align(
											alignment: Alignment.bottomLeft,
											child: widget.leftButtons,
										),
									],
								),
							),
						);
					},
				),
			),
			const SliverToBoxAdapter(
				child: SizedBox(height: sliverHeaderBottomPadding),
			),
		];

		if (widget.body != null) {
			slivers.add(widget.body!);
		}

		return SafeArea(
			top: false,
			bottom: false,
			child: CustomScrollView(
				controller: widget.controller,
				physics: widget.scrollPhysics ?? const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
				slivers: slivers,
			),
		);
	}
}
