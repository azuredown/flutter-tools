
import 'dart:async';

import 'package:custom_tools/BasicExtensions.dart';
import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/Startup.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:tuple/tuple.dart';

Map<String, dynamic>? _values;
Map<String, Tuple2<double, double>>? _bounds;

Future<void> initRemoteConfig({required Map<String, dynamic> values, Map<String, Tuple2<double, double>>? bounds, void Function()? listener}) async {
	_values = values;
	_bounds = bounds;
	if (remoteConfigEnabled) {
		try {
			await FirebaseRemoteConfig.instance.setDefaults(values).then((_) async {
				// Throws an error on Mac
				if (!isMacOS()) {
					// This should not be awaited because it has a tendency to throw errors
					await FirebaseRemoteConfig.instance.fetchAndActivate().onError((Error error, StackTrace stackTrace) {
						crashlyticsRecordError("Error in remote config: $error", stackTrace);
						return false;
					});
				}

				if (listener != null && !kIsWeb) {
					FirebaseRemoteConfig.instance.onConfigUpdated.listen((_) {
						listener();
					});
				}
			});
		} on FirebaseException catch (e) {
			// This happens if there's no wifi
			if (e.code != "internal") {
				rethrow;
			}
		}
	}
}

/*
void smartAssert(bool assertCondition, [String? message]) {
	assert(assertCondition, message);
	if (!assertCondition) {
		crashlyticsRecordError(message, StackTrace.current);
	}
}
*/

bool? remoteConfigGetBool(String key) {
	if (_values == null) {
		return null;
	}
	if (remoteConfigEnabled) {
		return bool.tryParse(FirebaseRemoteConfig.instance.getValue(key).asString(), caseSensitive: false) ?? _values![key];
	} else {
		if (!_values!.containsKey(key)) {
			return null;
		}
		return _values![key];
	}
}

String? remoteConfigGetString(String key) {
	if (_values == null) {
		return null;
	}
	if (remoteConfigEnabled) {
		final String value = FirebaseRemoteConfig.instance.getValue(key).asString();
		return value == RemoteConfigValue.defaultValueForString ? _values![key] : value;
	} else {
		if (!_values!.containsKey(key)) {
			return null;
		}
		return _values![key];
	}
}

int? remoteConfigGetInt(String key) {

	int? initialValue;

	if (_values == null) {
		return null;
	}

	if (remoteConfigEnabled) {
		initialValue = int.tryParse(FirebaseRemoteConfig.instance.getValue(key).asString()) ?? _values![key];
	} else {
		if (!_values!.containsKey(key)) {
			return null;
		}
		initialValue = _values![key];
	}

	if (initialValue == null) {
		return null;
	}

	final Tuple2<double, double>? bounds = _bounds?[key];
	if (bounds != null) {
		assert(bounds.item1 <= initialValue && bounds.item2 >= initialValue, "$initialValue is not in range $bounds");
	}
	return initialValue.bound(bounds);
}

double? remoteConfigGetDouble(String key) {

	double? initialValue;

	if (_values == null) {
		return null;
	}

	if (remoteConfigEnabled) {
		initialValue = double.tryParse(FirebaseRemoteConfig.instance.getValue(key).asString()) ?? _values![key];
	} else {
		if (!_values!.containsKey(key)) {
			return null;
		}
		if (_values![key] is int) {
			initialValue = _values![key].toDouble();
		} else {
			initialValue = _values![key];
		}
	}

	if (initialValue == null) {
		return null;
	}

	final Tuple2<double, double>? bounds = _bounds?[key];
	if (bounds != null) {
		assert(bounds.item1 <= initialValue && bounds.item2 >= initialValue, "$initialValue is not in range $bounds");
	}
	return initialValue.bound(bounds);
}
