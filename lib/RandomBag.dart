import 'dart:math';

import './BasicExtensions.dart';

class RandomBag<T> {

	factory RandomBag() {
		return RandomBag<T>.fromList(<T>[]);
	}

	RandomBag.fromList(this.items);

	List<T> items;

	int get count => items.length;
	bool get empty => items.isEmpty;
	bool get notEmpty => items.isNotEmpty;

	void clear() {
		items.clear();
	}

	void add(T item) {
		items.add(item);
	}

	// Gets a random item and removes it from the bag
	T getRandomItem(Random rng) {
		return items.removeAtFast(rng.nextInt(items.length));
	}

	// Gets a list of random items and remove them from the bag.
	List<T> getListOfRandomItems(int numItemsToGet, Random rng) {
		final List<T> returnList = <T>[];

		for (int i = 0; i < numItemsToGet; i++) {
			returnList.add(getRandomItem(rng));
		}

		return returnList;
	}

}
