
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Color primaryTextColour(bool darkModeEnabled) => darkModeEnabled ? Colors.white : Colors.black;

Color toastTextColour(bool darkModeEnabled) => !darkModeEnabled ? Colors.white : Colors.black;
Color toastBackgroundColour(bool darkModeEnabled) => darkModeEnabled ? Colors.white : Colors.black;

Color defaultBackgroundColour(bool darkModeEnabled) => darkModeEnabled ? const Color.fromRGBO(30, 30, 30, 1) : const Color.fromRGBO(245, 245, 245, 1);
