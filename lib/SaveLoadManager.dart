
import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:binary_codec/binary_codec.dart';
import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/Startup.dart';
import 'package:custom_tools/TimeAuthenticator.dart';
import 'package:custom_tools/UpdateRoot.dart';
import 'package:file/memory.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tuple/tuple.dart';

const int WRITING_TO_TEMP = 1, NOT_WRITING_TO_TEMP = 0;
Queue<Future<void> Function()> operationQueue = Queue<Future<void> Function()>();

bool locked = false;

Future<void> executeOperations() async {
	int attemptNum = 1;
	while (locked) {
		await Future<void>.delayed(Duration(milliseconds: 100 * attemptNum));
		if (attemptNum > 10) {
			crashlyticsRecordError("Failed to save after 10 attempts. Queue length: ${operationQueue.length}", null);
			return;
		}
		attemptNum++;
	}

	try {
		locked = true;

		while (operationQueue.isNotEmpty) {
			final Future<void> Function() func = operationQueue.removeFirst();
			await func();
		}
	} catch (e, stack) {
		crashlyticsRecordError("Save error: $e", stack);
	} finally {
		// Apparently the finally block isn't always working correctly for some reason, may be an issue with debug builds
		locked = false;
	}
	locked = false;
}

// getApplicationDocumentsDirectory() doesn't work on mac for some reason
Future<Directory> getDocumentsDirectoryWrapper() async {
	if (kIsWeb) {
		debugPrint("Using override directory");
		return Directory("");
	} else if (Platform.isMacOS && isTest()) {
		debugPrint("Directory testing not supported on mac");
		return Directory("");
	} else if (Platform.isWindows && !isTest()) {
		final Directory documentsDirectory = await getApplicationDocumentsDirectory();
		final Directory returnDirectory = Directory(join(documentsDirectory.path, "${packageInfo!.appName}_data"));
		if (!returnDirectory.existsSync()) {
			returnDirectory.createSync();
		}
		return returnDirectory;
	}
	return getApplicationDocumentsDirectory();
}

Future<void> deleteFile({
	required String unauthenticatedSaveName,
	required String authenticatedSaveName,
	required String backupSaveName,
	MemoryFileSystem? testFileSystem,
}) async {

	if (kIsWeb) {
		return;
	}

	final Directory directory = await getDocumentsDirectoryWrapper();

	void deleteFile(String path) {
		final File file = testFileSystem == null
				? File(path)
				: testFileSystem.file(path);

		if (file.existsSync()) {
			file.deleteSync();
		}
	}

	operationQueue.add(() async {
		deleteFile(join(directory.path, unauthenticatedSaveName));
		deleteFile(join(directory.path, "${unauthenticatedSaveName}temp"));
		deleteFile(join(directory.path, "${unauthenticatedSaveName}flag"));
		deleteFile(join(directory.path, authenticatedSaveName));
		deleteFile(join(directory.path, "${authenticatedSaveName}temp"));
		deleteFile(join(directory.path, "${authenticatedSaveName}flag"));
		deleteFile(join(directory.path, backupSaveName));
	});
	return executeOperations();

}

Future<void> saveToFile({
	required String unauthenticatedSaveName,
	required String authenticatedSaveName,
	required Uint8List dataToSave,
	required void Function() tryDisplaySavedBlockedMessage,
	void Function()? onSaveUnauthenticated,
	void Function()? onSaveAuthenticated,
	MemoryFileSystem? testFileSystem,
	bool authenticateTime = true,
}) async {

	if (rollbackProtectionActivated) {
		tryDisplaySavedBlockedMessage();
		return;
	}

	if (kIsWeb) {
		return;
	}

	operationQueue.add(() async {
		final Directory directory = await getDocumentsDirectoryWrapper();

		await saveOneFile(directory.path, dataToSave, unauthenticatedSaveName, testFileSystem);
		if (onSaveUnauthenticated != null) {
			onSaveUnauthenticated();
		}
	});

	if (!authenticateTime || timeIsValidAndTryReauthenticateTimeIfNot()) {
		operationQueue.add(() async {
			final Directory directory = await getDocumentsDirectoryWrapper();

			await saveOneFile(directory.path, dataToSave, authenticatedSaveName, testFileSystem);
			if (onSaveAuthenticated != null) {
				onSaveAuthenticated();
			}
		});
	}

	return executeOperations();
}

Future<void> saveOneFile(String directoryToSave, Uint8List data, String saveName, MemoryFileSystem? testFileSystem) async {

	if (directoryToSave != "") {
		final Directory directorySavingTo = Directory(directoryToSave);
		if (!directorySavingTo.existsSync()) {
			directorySavingTo.createSync();
		}
	}

	final String flagFileLocation = join(directoryToSave, "${saveName}flag");
	final String tempFileLocation = join(directoryToSave, "${saveName}temp");
	final String normalFileLocation = join(directoryToSave, saveName);

	final File flagFile = testFileSystem == null
			? File(flagFileLocation)
			: testFileSystem.file(flagFileLocation);
	await flagFile.writeAsBytes(Uint8List.fromList(<int>[WRITING_TO_TEMP]), flush: true);

	File dataFile = testFileSystem == null
			? File(tempFileLocation)
			: testFileSystem.file(tempFileLocation);
	await dataFile.writeAsBytes(data, flush: true);

	await flagFile.writeAsBytes(Uint8List.fromList(<int>[NOT_WRITING_TO_TEMP]), flush: true);

	dataFile = testFileSystem == null
			? File(normalFileLocation)
			: testFileSystem.file(normalFileLocation);
	await dataFile.writeAsBytes(data, flush: true);
}

// Milliseconds since save made, data, future saves found
// Leave save time key null if don't care about authenticating
Future<Tuple2<int?, Map<dynamic, dynamic>>?> readMostRecentValidSaveFile({
	required String unauthenticatedSaveName,
	required String authenticatedSaveName,
	required String backupSaveName,
	required int? saveTimeKey,
	MemoryFileSystem? testFileSystem,
}) async {

	final Directory directory = await getDocumentsDirectoryWrapper();

	Tuple2<int?, Map<dynamic, dynamic>>? readData;

	readData = await tryLoadData(directory, unauthenticatedSaveName, backupSaveName, saveTimeKey, testFileSystem, false);
	if (readData != null) {
		return readData;
	}

	readData = await tryLoadData(directory, authenticatedSaveName, backupSaveName, saveTimeKey, testFileSystem, true);
	if (readData != null) {
		return readData;
	}

	readData = await readIfValidAndMakeBackupSave(directory, backupSaveName, backupSaveName, saveTimeKey, testFileSystem, true);
	if (readData != null) {
		crashlyticsRecordError("Loading from failsafe data!", StackTrace.current);
		return readData;
	}

	return null;
}

Future<Tuple2<int?, Map<dynamic, dynamic>>?> tryLoadData(Directory directory, String saveFileName, String backupSaveName, int? saveTimeKey, MemoryFileSystem? testFileSystem, bool authenticatedSave) async {

	Tuple2<int?, Map<dynamic, dynamic>>? readData;
	final int? flagData = await getFirstByte(testFileSystem == null
			? File(join(directory.path, "${saveFileName}flag"))
			: testFileSystem.file(join(directory.path, "${saveFileName}flag")));

	if (flagData != null && flagData == NOT_WRITING_TO_TEMP) {
		readData = await readIfValidAndMakeBackupSave(directory, "${saveFileName}temp", backupSaveName, saveTimeKey, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
		readData = await readIfValidAndMakeBackupSave(directory, saveFileName, backupSaveName, saveTimeKey, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
	} else {
		readData = await readIfValidAndMakeBackupSave(directory, saveFileName, backupSaveName, saveTimeKey, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
		readData = await readIfValidAndMakeBackupSave(directory, "${saveFileName}temp", backupSaveName, saveTimeKey, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
	}

	return null;
}

Future<int?> getFirstByte(File file) async {
	if (file.existsSync()) {
		final List<int> data = List<int>.from(await file.readAsBytes());
		if (data.isNotEmpty) {
			return data[0];
		}
	}
	return null;
}

Future<Tuple2<int?, Map<dynamic, dynamic>>?> readIfValidAndMakeBackupSave(
		Directory mainDirectory,
		String fileName,
		String backupSaveName,
		int? saveTimeKey,
		MemoryFileSystem? testFileSystem,
		bool authenticatedSave,
	) async {

	final File file = testFileSystem == null
			? File(join(mainDirectory.path, fileName))
			: testFileSystem.file(join(mainDirectory.path, fileName));

	Tuple2<int?, Map<dynamic, dynamic>>? readData;
	if (file.existsSync()) {
		try {
			readData = await _readFileAndComputeTimeSinceSave(file, saveTimeKey);
		} catch (e, stack) {
			crashlyticsRecordError("$fileName save is corrupted! Error: $e", stack);
			return null;
		}
	}

	if (readData != null && (saveTimeKey == null || readData.item1 == null || readData.item1! >= 0 || authenticatedSave)) {
		if (fileName != backupSaveName) {
			unawaited(file.copy(join(mainDirectory.path, backupSaveName)));
		}
		return readData;
	} else {
		return null;
	}

}

// Milliseconds since save made, data, future saves found
// This (in very very rare cases) can throw the error:
// Error: Invalid argument(s): No more elements (ArgumentError) list size: 0. Error thrown null.
Future<Tuple2<int?, Map<dynamic, dynamic>>> _readFileAndComputeTimeSinceSave(File file, int? saveTimeKey) async {

	final Uint8List byteList = await file.readAsBytes();

	// This (in very very rare cases) can throw the error:
	// Error: Invalid argument(s): No more elements (ArgumentError) list size: 0. Error thrown null.
	final Map<dynamic, dynamic> data = binaryCodec.decode(byteList);

	if (data[saveTimeKey] == null) {
		return Tuple2<int?, Map<dynamic, dynamic>>(null, data);
	}

	// This section is just for Samsung users who report incorrect datestamps. If that's the case try it again and if that doesn't work try an ntp server
	final DateTime firstTime = DateTime.now();
	final DateTime fileTime = DateTime.fromMillisecondsSinceEpoch(data[saveTimeKey]);
	int milliseconds = (firstTime.millisecondsSinceEpoch - data[saveTimeKey]).round();
	if (milliseconds < 0) {
		final DateTime secondTime = DateTime.now();
		milliseconds = (secondTime.millisecondsSinceEpoch - data[saveTimeKey]).round();

		if (milliseconds > 0) {
			crashlyticsRecordError("First DateTime check was incorrect ($firstTime - $fileTime)!", null);
		} else {
			final DateTime? ntpTime = await getNtpTime(firstOnly: true);

			if (ntpTime != null) {
				milliseconds = (ntpTime.millisecondsSinceEpoch - data[saveTimeKey]).round();

				if (milliseconds > 0) {
					crashlyticsRecordError("Fixed DateTime using Ntp time! ($firstTime - $fileTime)", null);
				} else {
					crashlyticsRecordError("Failed to get correct time. ($firstTime - fileTime)", null);
				}
			}
		}
	}

	// Used to use file modified time stamp but some devices use seconds instead of milliseconds for some reason
	// so never trust the modified time stamp
	return Tuple2<int, Map<dynamic, dynamic>>(milliseconds, data);
}

// For loading if we don't care about authenticated saves
Future<dynamic> readSaveFileOrBackup({
	required String saveName,
	required String backupSaveName,
	MemoryFileSystem? testFileSystem,
}) async {

	if (isTest() && testFileSystem == null) {
		return;
	}

	final Directory directory = await getDocumentsDirectoryWrapper();

	dynamic readData;

	readData = await tryLoadDataBasic(directory, saveName, backupSaveName, testFileSystem, true);
	if (readData != null) {
		return readData;
	}

	readData = await readIfValidAndMakeBackupSaveBasic(directory, backupSaveName, backupSaveName, testFileSystem, true);
	if (readData != null) {
		crashlyticsRecordError("Loading from failsafe data!", StackTrace.current);
		return readData;
	}

	return null;
}

// Milliseconds since save made, data, future saves found
// Leave save time key null if don't care about authenticating
Future<dynamic> readMostRecentValidSaveFileBasic({
	required String unauthenticatedSaveName,
	required String backupSaveName,
	required int? saveTimeKey,
	MemoryFileSystem? testFileSystem,
}) async {

	final Directory directory = await getDocumentsDirectoryWrapper();

	dynamic readData;

	readData = await tryLoadDataBasic(directory, unauthenticatedSaveName, backupSaveName, testFileSystem, false);
	if (readData != null) {
		return readData;
	}

	readData = await readIfValidAndMakeBackupSaveBasic(directory, backupSaveName, backupSaveName, testFileSystem, true);
	if (readData != null) {
		crashlyticsRecordError("Loading from failsafe data!", StackTrace.current);
		return readData;
	}

	return null;
}

// Basic version of saving without any time stamp checking. Allows us to return dynamic and not Tuple2<int?, Map<dynamic, dynamic>>
Future<dynamic> tryLoadDataBasic(Directory directory, String saveFileName, String backupSaveName, MemoryFileSystem? testFileSystem, bool authenticatedSave) async {

	dynamic readData;
	final int? flagData = await getFirstByte(testFileSystem == null
			? File(join(directory.path, '${saveFileName}flag'))
			: testFileSystem.file(join(directory.path, '${saveFileName}flag')));

	if (flagData != null && flagData == NOT_WRITING_TO_TEMP) {
		readData = await readIfValidAndMakeBackupSaveBasic(directory, "${saveFileName}temp", backupSaveName, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
		readData = await readIfValidAndMakeBackupSaveBasic(directory, saveFileName, backupSaveName, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
	} else {
		readData = await readIfValidAndMakeBackupSaveBasic(directory, saveFileName, backupSaveName, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
		readData = await readIfValidAndMakeBackupSaveBasic(directory, "${saveFileName}temp", backupSaveName, testFileSystem, authenticatedSave);
		if (readData != null) {
			return readData;
		}
	}

	return null;
}

Future<dynamic> readIfValidAndMakeBackupSaveBasic(
		Directory mainDirectory,
		String fileName,
		String backupSaveName,
		MemoryFileSystem? testFileSystem,
		bool authenticatedSave,
		) async {

	final File file = testFileSystem == null
			? File(join(mainDirectory.path, fileName))
			: testFileSystem.file(join(mainDirectory.path, fileName));

	dynamic readData;
	if (file.existsSync()) {
		try {
			readData = binaryCodec.decode(await file.readAsBytes());
		} catch (e) {
			crashlyticsRecordError("$fileName save is corrupted! Error: $e", StackTrace.current);
			return null;
		}
	}

	if (readData != null) {
		if (fileName != backupSaveName) {
			unawaited(file.copy(join(mainDirectory.path, backupSaveName)));
		}
		return readData;
	} else {
		return null;
	}
}
