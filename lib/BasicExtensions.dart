
import 'dart:math' as math;
import 'dart:math';

import 'package:custom_tools/RandomBag.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tuple/tuple.dart';

import 'BasicFunctions.dart';

extension BoundInt on int {
	int bound(Tuple2<double, double>? data) {
		if (data == null) {
			return this;
		}
		assert(data.item2 >= data.item1);
		if (data.item1 > this) {
			return data.item1.round();
		} else if (data.item2 < this) {
			return data.item2.round();
		} else {
			return this;
		}
	}
}

extension BoundDouble on double {
	double bound(Tuple2<double, double>? data) {
		if (data == null) {
			return this;
		}
		assert(data.item2 >= data.item1);
		if (data.item1 > this) {
			return data.item1;
		} else if (data.item2 < this) {
			return data.item2;
		} else {
			return this;
		}
	}
}

extension FastOperations<T> on List<T> {
	T removeAtFast(int index) {
		final T item = this[index];
		this[index] = this[length - 1];
		removeAt(length - 1);
		return item;
	}
}

extension ThemeDarkMode on ThemeData {
	bool get darkModeEnabled => brightness == Brightness.dark;
}

bool isDarkMode(ThemeData data) {
	return data.darkModeEnabled;
}

// iOS doesn't need this, but Android does
SystemUiOverlayStyle makeUiOverlayStyle(bool whiteIcons) {
	return SystemUiOverlayStyle(
		// Must do this otherwise iOS icon colours won't update properly.
		statusBarBrightness: whiteIcons ? Brightness.dark : Brightness.light,

		// Must do this otherwise Android icon colours won't update properly.
		statusBarIconBrightness: whiteIcons ? Brightness.light : Brightness.dark,
	);
}

extension Capitalization on String {
	String convertToTitleCase() {
		final StringBuffer buffer = StringBuffer();

		bool armed = true;
		for (int i = 0; i < length; i++) {
			if (armed) {
				armed = false;
				buffer.write(this[i].toUpperCase());
			} else {
				if (this[i] == " ") {
					armed = true;
				}
				buffer.write(this[i]);
			}
		}

		return buffer.toString();
	}

	String capitalizeFirstLetter() {
		if (isEmpty) {
			return "";
		} else {
			return "${this[0].toUpperCase()}${substring(1)}";
		}
	}

	String ensureEndsWithPeriod() {
		if (isEmpty) {
			return "";
		} else {
			if (this[length - 1] != ".") {
				return "$this.";
			} else {
				return this;
			}
		}
	}
}

extension AmPm on DateTime {
	String toStringAmPm() {
		final bool pm = hour >= 12;
		int correctedHour = hour % 12;
		if (correctedHour == 0) {
			correctedHour = 12;
		}
		return "$correctedHour:${minute.toString().padLeft(2, "0")} ${pm ? "PM" : "AM"}";
	}
}

extension StringOperations on DateTime? {

	(bool inFuture, String duration)? englishHumanReadableDuration({DateTime? testingTime}) {
		if (this == null) {
			return null;
		}

		final Duration difference = (testingTime ?? DateTime.now()).difference(this!);

		final bool inFuture = difference.isNegative;
		final int minutesDuration = inFuture ? -difference.inMinutes + 1 : difference.inMinutes;

		if (minutesDuration == 0) {
			return (inFuture, "under a minute");
		} else if (minutesDuration == 1) {
			return (inFuture, "1 minute");
		} else if (minutesDuration >= 60) {
			final int hoursDuration = minutesDuration ~/ 60;
			if (hoursDuration == 1) {
				return (inFuture, "1 hour");
			} else if (hoursDuration >= 24) {
				final int daysDuration = hoursDuration ~/ 24;
				if (daysDuration == 1) {
					return (inFuture, "1 day");
				} else if (daysDuration >= 7) {
					final int yearsDuration = daysDuration ~/ 365;
					final int weeksDuration = daysDuration ~/ 7;
					final int monthsDuration = daysDuration ~/ 30;
					if (yearsDuration == 1) {
						return (inFuture, "1 year");
					} else if (yearsDuration > 1) {
						return (inFuture, "$yearsDuration years");
					} else if (monthsDuration == 1) {
						return (inFuture, "1 month");
					} else if (monthsDuration > 1) {
						return (inFuture, "$monthsDuration months");
					} else if (weeksDuration == 1) {
						return (inFuture, "1 week");
					} else {
						return (inFuture, "$weeksDuration weeks");
					}
				} else {
					return (inFuture, "$daysDuration days");
				}
			} else {
				return (inFuture, "$hoursDuration hours");
			}
		} else {
			return (inFuture, "$minutesDuration minutes");
		}
	}

	String englishHumanReadableDurationFromNow({DateTime? testingTime}) {

		final (bool inFuture, String duration)? duration = englishHumanReadableDuration(testingTime: testingTime);

		if (duration == null) {
			return "never";
		}

		if (duration.$1) {
			return "in ${duration.$2}";
		} else {
			return "${duration.$2} ago";
		}
	}
}

extension StringOperationsDuration on Duration {
	String englishHumanReadableRoundDown() {

		final int seconds = inSeconds;
		final double minutes = seconds / 60;
		final double hours = minutes / 60;
		final double days = hours / 24;

		if (hours >= 24) {
			if (days < 2) {
				return "1 day";
			} else {
				return "${days.floor()} days";
			}
		} else if (minutes >= 60) {
			if (hours < 2) {
				return "1 hour";
			} else {
				return "${hours.floor()} hours";
			}
		} else if (seconds >= 60) {
			if (minutes < 2) {
				return "1 minute";
			} else {
				return "${minutes.floor()} minutes";
			}
		} else {
			if (seconds == 1) {
				return "1 second";
			} else {
				return "$seconds seconds";
			}
		}
	}
}

extension Scramble<T> on List<T> {
	List<T> scramble(Random rng) {
		final RandomBag<T> bag = RandomBag<T>.fromList(this);
		return bag.getListOfRandomItems(length, rng);
	}
}

extension Math on List<int> {

	int get sum {
		int counter = 0;
		for (int i = 0; i < length; i++) {
			counter += this[i];
		}
		return counter;
	}

	double get average {
		if (isEmpty) {
			return double.nan;
		}
		return sum / length;
	}

	int get min {
		int minValue = this[0];
		for (int i = 1; i < length; i++) {
			minValue = math.min(minValue, this[i]);
		}
		return minValue;
	}

	int get max {
		int maxValue = this[0];
		for (int i = 1; i < length; i++) {
			maxValue = math.max(maxValue, this[i]);
		}
		return maxValue;
	}
}

extension Formatting on BuildContext {
	double get bottomPadding {
		return MediaQuery.of(this).padding.bottom * (isIOS() ? 0.75 : 1);
	}
}
