
import 'dart:async';
import 'dart:isolate';

import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/RetentionLogger.dart';
import 'package:custom_tools/TimeAuthenticator.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:is_ios_app_on_mac/is_ios_app_on_mac.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

String? get deviceUuid => isTest() ? "Test" : prefs.getString("DeviceUUID");
void setDeviceUuid(String value) {
	unawaited(prefs.setString("DeviceUUID", value));
}

late String gitHash;
late String flutterVersion;
late SharedPreferences prefs;
PackageInfo? packageInfo;
int? initialBuildNumber;
bool iOsRunningOnMac = false;

bool firebaseEnabled = !isTest();
bool firebaseAuthEnabled = firebaseEnabled && !isWindows();
bool remoteConfigEnabled = firebaseEnabled && !isWindows();
bool firebaseAnalyticsEnabled = firebaseEnabled && !isWindows();
bool crashlyticsEnabled = firebaseEnabled && !isWindows() && !kIsWeb;
bool appCheckEnabled = firebaseEnabled && !isWindows();
bool firebaseMessagingEnabled = firebaseEnabled && !isWindows();
bool firebasePerfEnabled = firebaseEnabled && !isWindows() && !isMacOS();
bool firebaseCloudStorageEnabled = firebaseEnabled && !isWindows(); // It says that Windows should be supported but I'm getting errors

// Should be called from main
Future<void> onAppStart(Future<Widget> Function() makeApp, FirebaseOptions? firebaseOptions, {
	bool allowRunningWithoutFirebase = kDebugMode,
	SystemUiOverlayStyle defaultUiOverlayStyle = const SystemUiOverlayStyle(
		systemNavigationBarColor: Colors.transparent,
		statusBarColor: Colors.transparent,
	),
	String? webCaptchaKey}) async {

	await runZonedGuarded<Future<void>>(() async {
		WidgetsFlutterBinding.ensureInitialized();

		await SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
		SystemChrome.setSystemUIOverlayStyle(
			defaultUiOverlayStyle,
		);

		await preInit(firebaseOptions, allowRunningWithoutFirebase: allowRunningWithoutFirebase, webCaptchaSiteKey: webCaptchaKey);

		runApp(await makeApp());
	}, (Object error, StackTrace stack) => crashlyticsRecordError(error, stack));
}
Future<void> onAppStartWithoutPreInit(Future<Widget> Function() makeApp, {
	SystemUiOverlayStyle defaultUiOverlayStyle = const SystemUiOverlayStyle(
		systemNavigationBarColor: Colors.transparent,
		statusBarColor: Colors.transparent,
	)}) async {

	await runZonedGuarded<Future<void>>(() async {
		WidgetsFlutterBinding.ensureInitialized();

		await SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
		SystemChrome.setSystemUIOverlayStyle(
			defaultUiOverlayStyle,
		);

		runApp(await makeApp());
	}, (Object error, StackTrace stack) => crashlyticsRecordError(error, stack));
}

Future<void> preInit(FirebaseOptions? firebaseOptions, {bool allowRunningWithoutFirebase = kDebugMode, String? webCaptchaSiteKey}) async {
	if (firebaseEnabled) {
		try {
			await Firebase.initializeApp(
				options: firebaseOptions,
			);
		} catch (e, stack) {
			if (allowRunningWithoutFirebase) {
				// This throws 'PlatformException(channel-error, Unable to establish connection on channel., null, null)' sometimes
				crashlyticsRecordError(e, stack);
				firebaseEnabled = false;
			} else {
				rethrow;
			}
		}
	}

	if (appCheckEnabled) {
		if (kIsWeb) {
			if (webCaptchaSiteKey != null) {
				// Must be called before any firebase usages
				await FirebaseAppCheck.instance.activate(webProvider: ReCaptchaV3Provider(webCaptchaSiteKey));
			}
		} else if (kDebugMode) {
			await FirebaseAppCheck.instance.activate(androidProvider: AndroidProvider.debug, appleProvider: AppleProvider.debug);
		} else {
			await FirebaseAppCheck.instance.activate();
		}
	}

	// Flutter web does not support isolates
	if (!kIsWeb) {
		Isolate.current.addErrorListener(RawReceivePort((List<dynamic> pair) async {
			final List<dynamic> errorAndStacktrace = pair;
			await FirebaseCrashlytics.instance.recordError(
				errorAndStacktrace.first,
				errorAndStacktrace.last,
			);
		}).sendPort);
	}

	FlutterError.onError = (FlutterErrorDetails details) {
		if (!crashlyticsEnabled) {
			crashlyticsRecordError(details.exception, details.stack);
		} else {
			FirebaseCrashlytics.instance.recordFlutterError(details);
		}
	};

	prefs = await SharedPreferences.getInstance();
}

Future<void> mainInit({
	required bool initTimeServer,
	bool debugCrashlyticsEnabled = false,
	bool debugAnalyticsEnabled = false,
	void Function(int day)? dayCallback,
}) async {

	if (kDebugMode && !kIsWeb) {
		if (crashlyticsEnabled) {
			await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(debugCrashlyticsEnabled);
		}
		if (firebaseAnalyticsEnabled) {
			await FirebaseAnalytics.instance.setAnalyticsCollectionEnabled(debugAnalyticsEnabled);
		}
	}

	if (isIOS()) {
		unawaited(IsIosAppOnMac().isiOSAppOnMac().then((bool value) {
			iOsRunningOnMac = value;
		}));
	}

	if (deviceUuid == null) {
		setDeviceUuid(const Uuid().v4());
	}

	// Must be before remote config
	packageInfo = await PackageInfo.fromPlatform();
	initialBuildNumber = prefs.getInt("initialBuild");
	// Empty build number means unsupported
	if (initialBuildNumber == null && !kIsWeb && firebaseEnabled && packageInfo!.buildNumber != "") {
		try {
			initialBuildNumber = int.parse(packageInfo!.buildNumber);
			if (initialBuildNumber != null) {
				await prefs.setInt("initialBuild", initialBuildNumber!);
			}
		} catch (e) {
			crashlyticsRecordError("Build number is not an int: ${packageInfo!.buildNumber}", StackTrace.current);
		}
	}

	logRetention(dayCallback: dayCallback);

	if (firebaseAnalyticsEnabled) {
		unawaited(FirebaseAnalytics.instance.setUserProperty(name: "platform", value: isIOS() ? "iOS" : (isAndroid() ? "Android" : "other")));
		unawaited(FirebaseAnalytics.instance.setUserProperty(name: "initial_build", value: initialBuildNumber.toString()));
		unawaited(FirebaseAnalytics.instance.setUserProperty(name: "current_build", value: packageInfo?.buildNumber));
	}

	try {
		final List<String> fileContent = (await rootBundle.loadString('assets/GitInfo.txt')).split("\n");
		gitHash = fileContent[0].substring(0, 8);
		if (fileContent.length >= 2) {
			flutterVersion = fileContent[1];
		} else {
			flutterVersion = "";
		}
	} catch (e) {
		gitHash = "";
		flutterVersion = "";
		crashlyticsRecordError("Git has not initialized properly!", StackTrace.current);
	}

	crashlyticsLogEvent("Initial build number: $initialBuildNumber");

	if (initTimeServer) {
		unawaited(authenticateTime());
	}

	if (firebaseAnalyticsEnabled) {
		unawaited(FirebaseAnalytics.instance.logAppOpen());
	}
}
