
import 'dart:async';

import 'package:custom_tools/BasicFunctions.dart';
import 'package:flutter/scheduler.dart';

/*
 *
 *	Allows scheduling an update callback to be called every frame as well as
 *  rollback protection (system time goes backwards by a significant amount)
 *
 */

Future<void> initTickFunctionFrameSynced(void Function(double milliseconds) tickAction, {int Function()? framesBetweenTicks}) {
	if (_tickAction != null) {
		throw Exception("Duplicate tick!");
	}
	if (framesBetweenTicks != null) {
		_framesBetweenTicks = framesBetweenTicks;
	}
	_tickAction = tickAction;
	_previousTime = DateTime.now();
	return Ticker(_tick).start();
}

void initTickFunctionCustomInterval(void Function(double milliseconds) tickAction, {Duration duration = const Duration(milliseconds: 50)}) {
	if (_tickAction != null) {
		throw Exception("Duplicate tick!");
	}
	_tickAction = tickAction;
	_previousTime = DateTime.now();
	Timer.periodic(duration, (Timer timer) {
		_tick(DateTime.now().difference(_previousTime));
		_previousTime = DateTime.now();
	});
}

int Function()? _framesBetweenTicks;
void Function(double milliseconds)? _tickAction;
late DateTime _previousTime;
int currentFramesBetweenTicks = 0;

// Triggered if we roll back by over 24 hours. Is reset upon restarting.
bool rollbackProtectionActivated = false;

void _tick(_) {

	if (currentFramesBetweenTicks <= 0) {
		currentFramesBetweenTicks = _framesBetweenTicks == null ? 0 : _framesBetweenTicks!();
	} else {
		currentFramesBetweenTicks--;
		return;
	}

	final DateTime now = DateTime.now();
	final double deltaMilliseconds = now.difference(_previousTime).inMicroseconds / 1000;
	_previousTime = now;

	if (deltaMilliseconds < -24 * 60 * 60 * 1000) {
		rollbackProtectionActivated = true;
		crashlyticsRecordError("Rollback protection activated", null);
	}

	_tickAction!(deltaMilliseconds);

}
