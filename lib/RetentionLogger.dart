
import 'dart:math';

import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';

void logRetention({DateTime? now, void Function(int day)? dayCallback}) {

	now ??= DateTime.now();

	final int? appStartTimeSinceEpoch = prefsGetInt("AppStartTime");
	final DateTime appStartTime;

	if (appStartTimeSinceEpoch == null) {
		prefsSetInt("AppStartTime", now.millisecondsSinceEpoch);
		appStartTime = now;
	} else {
		appStartTime = DateTime.fromMillisecondsSinceEpoch(appStartTimeSinceEpoch);
	}

	final int days = now.difference(appStartTime).inDays;

	// Day 0 is treated as a special case because it breaks my exponential function
	if (prefsGetBool("logged_d0") == null) {
		prefsSetBool("logged_d0", true);
		analyticsLogEvent("d0");
	}

	if (days < 0) {
		crashlyticsRecordError("Days is <= 0!", null);
		return;
	}

	if (dayCallback != null) {
		dayCallback(days);
	}

	if (days == 0) {
		return;
	}

	final int maxLog = min((log(days)/log(2)).floor(), 9);

	for (int i = 0; i <= maxLog; i++) {
		final int day = pow(2, i).toInt();
		if (prefsGetBool("logged_d$day") == null) {
			prefsSetBool("logged_d$day", true);
			analyticsLogEvent("d$day");
		}
	}
}
