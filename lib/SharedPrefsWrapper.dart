
import 'dart:async';

import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/Startup.dart';

Map<String, dynamic> testPrefs = <String, dynamic>{};

double? prefsGetDouble(String key) {
	if (isTest()) {
		return testPrefs[key];
	} else {
		return prefs.getDouble(key);
	}
}

void prefsSetDouble(String key, double value) {
	if (isTest()) {
		testPrefs[key] = value;
	} else {
		unawaited(prefs.setDouble(key, value));
	}
}

int? prefsGetInt(String key) {
	if (isTest()) {
		return testPrefs[key];
	} else {
		return prefs.getInt(key);
	}
}

void prefsSetInt(String key, int value) {
	if (isTest()) {
		testPrefs[key] = value;
	} else {
		unawaited(prefs.setInt(key, value));
	}
}

bool? prefsGetBool(String key) {
	if (isTest()) {
		return testPrefs[key];
	} else {
		return prefs.getBool(key);
	}
}

void prefsSetBool(String key, bool value) {
	if (isTest()) {
		testPrefs[key] = value;
	} else {
		unawaited(prefs.setBool(key, value));
	}
}

String? prefsGetString(String key) {
	if (isTest()) {
		return testPrefs[key];
	} else {
		return prefs.getString(key);
	}
}

void prefsSetString(String key, String value) {
	if (isTest()) {
		testPrefs[key] = value;
	} else {
		unawaited(prefs.setString(key, value));
	}
}
