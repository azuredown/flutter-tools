
import 'dart:async';
import 'dart:io';

import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:ntp/ntp.dart';

DateTime? lastAuthenticationAttempt;

bool timeIsValidAndTryReauthenticateTimeIfNot() {
	final int? time = prefsGetInt("LastAuthenticatedTime");
	if (time == null ||
			DateTime.now().millisecondsSinceEpoch > time + 1000 * 60 * 60 * 3 ||
			DateTime.now().millisecondsSinceEpoch < time - 1000 * 60 * 60 * 6) {
		unawaited(authenticateTime());
		return false;
	} else {

		// Reauthenticate time if we're going to expire soon
		if (DateTime.now().millisecondsSinceEpoch > time + 1000 * 60 * 60 * 2) {
			unawaited(authenticateTime());
		}

		return true;
	}
}

Future<void> authenticateTime() async {

	if (lastAuthenticationAttempt != null && lastAuthenticationAttempt!.difference(DateTime.now()).inSeconds.abs() < 60) {
		return;
	}
	lastAuthenticationAttempt = DateTime.now();

	final DateTime? timeServerTime = await getNtpTime();
	if (timeServerTime != null) {
		prefsSetInt("LastAuthenticatedTime", timeServerTime.millisecondsSinceEpoch);
	}
}

Future<DateTime?> getNtpTime({bool firstOnly = false}) async {
	final List<dynamic> errors = <dynamic>[];
	try {
		final DateTime now = await NTP.now(lookUpAddress: "time.nist.gov", timeout: const Duration(seconds: 5));
		return now;
	} catch (error) {
		if (error is! TimeoutException) {
			errors.add(error);
		}
	}
	try {
		final DateTime now = await NTP.now(lookUpAddress: "pool.ntp.org", timeout: const Duration(seconds: 5));
		return now;
	} catch (error) {
		if (error is! TimeoutException) {
			errors.add(error);
		}
	}
	if (firstOnly) {
		return null;
	}
	try {
		final DateTime now = await NTP.now(lookUpAddress: "time.cloudflare.com", timeout: const Duration(seconds: 5));
		return now;
	} catch (error) {
		if (error is! TimeoutException) {
			errors.add(error);
		}
	}
	try {
		final DateTime now = await NTP.now(lookUpAddress: "ntp1.net.berkeley.edu", timeout: const Duration(seconds: 5));
		return now;
	} catch (error) {
		if (error is! TimeoutException) {
			errors.add(error);
		}
	}

	if (errors.isNotEmpty) {
		try {
			final List<InternetAddress> result = await InternetAddress.lookup("example.com");
			if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
				crashlyticsRecordError("All time servers failed. Error: $errors", StackTrace.current);
			}
		} on SocketException catch (_) {
		}
	}

	if (isTest()) {
		crashlyticsRecordError("No Internet connection!", null);
		return DateTime.now();
	}

	return null;
}
