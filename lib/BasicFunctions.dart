
import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:custom_tools/Startup.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:tuple/tuple.dart';
import 'package:url_launcher/url_launcher.dart';

int numErrorsToIgnoreForTesting = 0;

// For some reason a blank window appears for a split second on iOS apps running on mac from a web view
LaunchMode defaultLaunchMode() => isAndroid() || iOsRunningOnMac ? LaunchMode.externalApplication : LaunchMode.platformDefault;

enum BrightnessStatus {
	DEVICE,
	LIGHT,
	DARK,
}

Future<void> launchURLWrapper(String URL, {LaunchMode? launchMode}) async {
	final bool successful = await launchUrl(Uri.parse(URL),	mode: launchMode ?? defaultLaunchMode());

	// On web successful is always false for some reason
	if (!successful && !kIsWeb) {
		crashlyticsRecordError("Failed to launch $URL", StackTrace.current);
	}
}

void analyticsLogEvent(String event, {Map<String, Object>? parameters}) {
	if (Firebase.apps.isEmpty || !firebaseAnalyticsEnabled) {
		if (!isTest()) {
			debugPrint("Analytics event: $event");
		}
	} else {

		if (parameters != null) {
			final List<String> keys = parameters.keys.toList();
			for (final String key in keys) {
				if (parameters[key] == null) {
					parameters.remove(key);
				}
			}
		}

		return unawaited(FirebaseAnalytics.instance.logEvent(name: event, parameters: parameters));
	}
}

void crashlyticsRecordErrorThrottled(String message, StackTrace? stack, {
	List<String>? errors, Duration throttlingDuration = const Duration(days: 1)}) {

	final int? timeStamp = prefsGetInt("crashlytics_$message");

	if (timeStamp == null) {
		crashlyticsRecordError(message, stack, errors: errors);
		prefsSetInt("crashlytics_$message", DateTime.now().millisecondsSinceEpoch);
		return;
	}

	final DateTime lastMessageTime = DateTime.fromMillisecondsSinceEpoch(timeStamp);
	if (DateTime.now().isAfter(lastMessageTime.add(throttlingDuration))) {
		crashlyticsRecordError(message, stack, errors: errors);
		prefsSetInt("crashlytics_$message", DateTime.now().millisecondsSinceEpoch);
		return;
	}
}

void crashlyticsRecordError(dynamic message, StackTrace? stack, {List<String>? errors}) {
	if (isTest()) {
		numErrorsToIgnoreForTesting--;
		if (numErrorsToIgnoreForTesting >= 0) {
			return;
		}
	}

	if (!crashlyticsEnabled || Firebase.apps.isEmpty) {
		if (stack != null) {
			debugPrint("Encountered error: $message\nStack:\n$stack");
		} else {
			debugPrint("Encountered error: $message");
		}
	} else {
		if (errors == null) {
			// Crashlytics is not supported on web
			if (kIsWeb) {
				debugPrint(message);
			} else {
				unawaited(FirebaseCrashlytics.instance.recordError(message, stack));
			}
		} else {
			errors.add(message);
		}
	}
}

Future<void> tryPromptForReview({int? maxNumPromptsAllowed = 1}) async {

	final int numPromptsMade = prefs.getInt("ReviewPromptsMade") ?? 0;

	if (maxNumPromptsAllowed != null && numPromptsMade >= maxNumPromptsAllowed) {
		return;
	}

	final InAppReview inAppReview = InAppReview.instance;
	await inAppReview.isAvailable().then((bool available) {
		if (available) {
			inAppReview.requestReview();
			analyticsLogEvent("prompted_for_review");
			unawaited(prefs.setInt("ReviewPromptsMade", numPromptsMade + 1));
		}
	});
}

void crashlyticsLogEvent(String message) {
	if (!crashlyticsEnabled || Firebase.apps.isEmpty) {
		if (!isTest()) {
			debugPrint("Logging event: $message");
		}
	} else {
		unawaited(FirebaseCrashlytics.instance.log(message));
	}
}

bool isApple() => isIOS() || isMacOS();

bool isIOS() {
	return !kIsWeb && Platform.isIOS;
}

bool isAndroid() {
	return !kIsWeb && Platform.isAndroid;
}

bool isWindows() {
	return !kIsWeb && !isTest() && Platform.isWindows;
}

bool isMacOS() {
	return !kIsWeb && !isTest() && Platform.isMacOS;
}

void showToast(String message, {bool longDuration = false}) {
	if (isWindows() || isMacOS() || isTest()) {
		debugPrint("Toast: $message");
	} else {
		unawaited(Fluttertoast.cancel().catchError((_) => false ).then((_) => Fluttertoast.showToast(msg: message, toastLength: longDuration ? Toast.LENGTH_LONG : null)));
	}
}

bool isTest() {
	if (kIsWeb || !kDebugMode) {
		return false;
	} else {
		return Platform.environment.containsKey('FLUTTER_TEST');
	}
}

double lerp(num start, num end, double progress) => start * (1 - progress) + end * progress;

Future<T> isolateWrapper<T>(FutureOr<T> Function() func) async {
	if (isTest() || kIsWeb) {
		return func();
	} else {
		return Isolate.run(func);
	}
}

Future<ReturnType> isolateWrapperLegacy<ReturnType, Arg>(Future<Tuple2<ReturnType, Tuple2<dynamic, String?>?>> Function(Arg) isolate, Arg arg) async {
	Tuple2<ReturnType, Tuple2<dynamic, String?>?> data;
	try {
		data = await compute(isolate, arg);
	} catch (e, stack) {
		unawaited(FirebaseCrashlytics.instance.recordError(e, stack));
		rethrow;
	}

	final Tuple2<dynamic, String?>? errorData = data.item2;
	if (errorData != null) {
		final String? stack = errorData.item2;
		if (stack == null) {
			// If stacktrace is not null we get 'E/FLTFirebaseCrashlytics( 7872): Unable to generate stack trace element from Dart error.' for some reason.
			// Also happens asynchronously which is weird.
			crashlyticsRecordError(errorData.item1, null);
		} else {
			// Gives 'E/FLTFirebaseCrashlytics( 7872): Unable to generate stack trace element from Dart error.' for some reason.
			// Also happens asynchronously which is weird.
			crashlyticsRecordError(errorData.item1, Trace.parse(stack));
		}
	}

	return data.item1;
}

SizedBox button(Widget text, Function? onPress, {Future<void>? waitingFor}) {
	return SizedBox(
		width: double.infinity,
		child: waitingFor == null
				? ElevatedButton(
			onPressed: onPress as void Function()?,
			child : text,
		)
				: FutureBuilder<void>(
				future: waitingFor,
				builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
					return ElevatedButton(
						onPressed: snapshot.connectionState != ConnectionState.done ? null : onPress as void Function()?,
						child: snapshot.connectionState != ConnectionState.done ? const SizedBox(
							height: 12,
							width: 12,
							child: CircularProgressIndicator(color: Colors.white),
						) : text,
					);
				}
		),
	);
}
