

import 'package:custom_tools/RetentionLogger.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

	setUp(() {
		// This has to be in setUp() not tearDown() for some reason
		testPrefs.clear();
	});

	test('logRetention setting start time', () async {
		logRetention(now: DateTime(2024, 1, 1));
		expect(testPrefs, <String, dynamic> { "AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch, "logged_d0" : true });
		logRetention(now: DateTime(2024, 1, 1));
		expect(testPrefs, <String, dynamic> { "AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch, "logged_d0" : true });
		logRetention(now: DateTime(2024, 1, 2));
		expect(testPrefs, <String, dynamic> { "AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch, "logged_d0" : true, 'logged_d1': true });
		testPrefs.clear();
		logRetention(now: DateTime(2024, 1, 2));
		expect(testPrefs, <String, dynamic> { "AppStartTime" : DateTime(2024, 1, 2).millisecondsSinceEpoch, "logged_d0" : true });
	});

	test('logRetention days', () async {
		logRetention(now: DateTime(2024, 1, 1));
		expect(testPrefs, <String, dynamic> { "AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch, "logged_d0" : true });
		logRetention(now: DateTime(2024, 1, 2));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
		});
		logRetention(now: DateTime(2024, 1, 3));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
		});
		logRetention(now: DateTime(2024, 1, 4));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
		});
		logRetention(now: DateTime(2024, 1, 5));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
		});
		logRetention(now: DateTime(2024, 1, 8));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
		});
		logRetention(now: DateTime(2024, 1, 9));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
			"logged_d8": true,
		});
		logRetention(now: DateTime(2024, 1, 16));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
			"logged_d8": true,
		});
		logRetention(now: DateTime(2024, 1, 17));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
			"logged_d8": true,
			"logged_d16": true,
		});
		logRetention(now: DateTime(2030, 1, 17));
		expect(testPrefs, <String, dynamic> {
			"AppStartTime" : DateTime(2024, 1, 1).millisecondsSinceEpoch,
			"logged_d0" : true,
			"logged_d1": true,
			"logged_d2": true,
			"logged_d4": true,
			"logged_d8": true,
			"logged_d16": true,
			"logged_d32": true,
			"logged_d64": true,
			"logged_d128": true,
			"logged_d256": true,
			"logged_d512": true,
		});
	});

	test('logRetention callback', () async {
		final Set<int> days = <int>{};
		logRetention(now: DateTime(2024, 1, 1), dayCallback: (int day) => days.add(day));
		expect(days, <int>{0});
		logRetention(now: DateTime(2024, 1, 2), dayCallback: (int day) => days.add(day));
		expect(days, <int>{0, 1});
		logRetention(now: DateTime(2024, 1, 10), dayCallback: (int day) => days.add(day));
		expect(days, <int>{0, 1, 9});
	});

}
