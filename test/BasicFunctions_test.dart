
import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

	tearDown(() {
		numErrorsToIgnoreForTesting = 0;
		testPrefs.clear();
	});

	test('Crashlytics basic', () {
		numErrorsToIgnoreForTesting = 1;
		crashlyticsRecordError("test", StackTrace.current);
		expect(numErrorsToIgnoreForTesting, 0);
	});

	test('Crashlytics throttled', () {
		numErrorsToIgnoreForTesting = 1;
		crashlyticsRecordErrorThrottled("test", StackTrace.current);
		expect(numErrorsToIgnoreForTesting, 0);

		numErrorsToIgnoreForTesting = 1;
		crashlyticsRecordErrorThrottled("test", StackTrace.current);
		expect(numErrorsToIgnoreForTesting, 1);
		expect(DateTime.fromMillisecondsSinceEpoch(testPrefs["crashlytics_test"]).isAfter(DateTime.now().subtract(const Duration(minutes: 1))), true);
		expect(DateTime.fromMillisecondsSinceEpoch(testPrefs["crashlytics_test"]).isBefore(DateTime.now().add(const Duration(minutes: 1))), true);

		testPrefs["crashlytics_test"] = DateTime.now().subtract(const Duration(hours: 23)).millisecondsSinceEpoch;

		numErrorsToIgnoreForTesting = 1;
		crashlyticsRecordErrorThrottled("test", StackTrace.current);
		expect(numErrorsToIgnoreForTesting, 1);

		testPrefs["crashlytics_test"] = DateTime.now().subtract(const Duration(hours: 25)).millisecondsSinceEpoch;

		numErrorsToIgnoreForTesting = 1;
		crashlyticsRecordErrorThrottled("test", StackTrace.current);
		expect(numErrorsToIgnoreForTesting, 0);
	});
}
