
import 'package:custom_tools/RemoteConfigWrapper.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tuple/tuple.dart';

void main() async {

	await initRemoteConfig(
		values: <String, dynamic>{
			"TestDouble": 5,
			"TestInt" : 5,
		},
		bounds: <String, Tuple2<double, double>>{
			"TestInt" : const Tuple2<double, double>(4, 7),
		},
	);

	test("Casting works properly", () async {
		expect(remoteConfigGetDouble("TestDouble"), 5);
	});

	test("Invalid values", () async {
		expect(remoteConfigGetDouble("DoesNotExist"), null);
		expect(remoteConfigGetInt("DoesNotExist"), null);
		expect(remoteConfigGetBool("DoesNotExist"), null);
		expect(remoteConfigGetString("DoesNotExist"), null);
	});

	test("Bounds", () async {
		expect(remoteConfigGetInt("TestInt"), 5);
	});
}
