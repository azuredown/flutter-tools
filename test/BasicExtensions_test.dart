
import 'package:custom_tools/BasicExtensions.dart';
import 'package:custom_tools/BasicFunctions.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tuple/tuple.dart';

void main() {

	tearDown(() {
		expect(numErrorsToIgnoreForTesting, 0);
	});

	test('toStringAmPm', () {
		expect(DateTime(2020, 1, 1, 0, 0).toStringAmPm(), "12:00 AM");
		expect(DateTime(2020, 1, 1, 0, 1).toStringAmPm(), "12:01 AM");
		expect(DateTime(2020, 1, 1, 0, 59).toStringAmPm(), "12:59 AM");
		expect(DateTime(2020, 1, 1, 1, 0).toStringAmPm(), "1:00 AM");
		expect(DateTime(2020, 1, 1, 11, 0).toStringAmPm(), "11:00 AM");
		expect(DateTime(2020, 1, 1, 11, 59).toStringAmPm(), "11:59 AM");
		expect(DateTime(2020, 1, 1, 12, 0).toStringAmPm(), "12:00 PM");
		expect(DateTime(2020, 1, 1, 12, 1).toStringAmPm(), "12:01 PM");
		expect(DateTime(2020, 1, 1, 23, 0).toStringAmPm(), "11:00 PM");
		expect(DateTime(2020, 1, 1, 23, 59).toStringAmPm(), "11:59 PM");
		expect(DateTime(2020, 1, 1, 24, 0).toStringAmPm(), "12:00 AM");
	});

	test('humanReadableDurationFromNow', () {
		final DateTime referenceTime = DateTime.now();
		expect(null.englishHumanReadableDurationFromNow(), "never");
		expect(referenceTime.englishHumanReadableDurationFromNow(testingTime: referenceTime), "under a minute ago");
		expect(referenceTime.subtract(const Duration(seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "under a minute ago");
		expect(referenceTime.subtract(const Duration(seconds: 60)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 minute ago");
		expect(referenceTime.subtract(const Duration(minutes: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "5 minutes ago");
		expect(referenceTime.subtract(const Duration(minutes: 59, seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "59 minutes ago");
		expect(referenceTime.subtract(const Duration(hours: 1)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 hour ago");
		expect(referenceTime.subtract(const Duration(hours: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "5 hours ago");
		expect(referenceTime.subtract(const Duration(hours: 23, minutes: 59, seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "23 hours ago");
		expect(referenceTime.subtract(const Duration(hours: 24)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 day ago");
		expect(referenceTime.subtract(const Duration(days: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "5 days ago");
		expect(referenceTime.subtract(const Duration(days: 7)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 week ago");
		expect(referenceTime.subtract(const Duration(days: 29)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "4 weeks ago");
		expect(referenceTime.subtract(const Duration(days: 30)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 month ago");
		expect(referenceTime.subtract(const Duration(days: 150)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "5 months ago");
		expect(referenceTime.subtract(const Duration(days: 365)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "1 year ago");
		expect(referenceTime.subtract(const Duration(days: 3650)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "10 years ago");

		expect(referenceTime.add(const Duration(seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 minute");
		expect(referenceTime.add(const Duration(seconds: 60)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 2 minutes");
		expect(referenceTime.add(const Duration(minutes: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 6 minutes");
		expect(referenceTime.add(const Duration(minutes: 59, seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 hour");
		expect(referenceTime.add(const Duration(hours: 1)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 hour");
		expect(referenceTime.add(const Duration(hours: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 5 hours");
		expect(referenceTime.add(const Duration(hours: 23, minutes: 59, seconds: 59)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 day");
		expect(referenceTime.add(const Duration(hours: 24)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 day");
		expect(referenceTime.add(const Duration(days: 5)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 5 days");
		expect(referenceTime.add(const Duration(days: 7)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 week");
		expect(referenceTime.add(const Duration(days: 29)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 4 weeks");
		expect(referenceTime.add(const Duration(days: 30)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 month");
		expect(referenceTime.add(const Duration(days: 150)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 5 months");
		expect(referenceTime.add(const Duration(days: 365)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 1 year");
		expect(referenceTime.add(const Duration(days: 3650)).englishHumanReadableDurationFromNow(testingTime: referenceTime), "in 10 years");
	});

	test('StringOperationsDuration', () {
		expect(Duration.zero.englishHumanReadableRoundDown(), "0 seconds");
		expect(const Duration(milliseconds: 500).englishHumanReadableRoundDown(), "0 seconds");
		expect(const Duration(seconds: 1).englishHumanReadableRoundDown(), "1 second");
		expect(const Duration(seconds: 1, milliseconds: 500).englishHumanReadableRoundDown(), "1 second");

		expect(const Duration(seconds: 59, milliseconds: 500).englishHumanReadableRoundDown(), "59 seconds");
		expect(const Duration(minutes: 1).englishHumanReadableRoundDown(), "1 minute");
		expect(const Duration(minutes: 1, seconds: 30).englishHumanReadableRoundDown(), "1 minute");
		expect(const Duration(minutes: 2).englishHumanReadableRoundDown(), "2 minutes");

		expect(const Duration(minutes: 59, seconds: 30).englishHumanReadableRoundDown(), "59 minutes");
		expect(const Duration(hours: 1).englishHumanReadableRoundDown(), "1 hour");
		expect(const Duration(hours: 1, minutes: 30).englishHumanReadableRoundDown(), "1 hour");
		expect(const Duration(hours: 2).englishHumanReadableRoundDown(), "2 hours");

		expect(const Duration(hours: 23, minutes: 30).englishHumanReadableRoundDown(), "23 hours");
		expect(const Duration(days: 1).englishHumanReadableRoundDown(), "1 day");
		expect(const Duration(days: 1, hours: 6).englishHumanReadableRoundDown(), "1 day");
		expect(const Duration(days: 2).englishHumanReadableRoundDown(), "2 days");
		expect(const Duration(days: 3).englishHumanReadableRoundDown(), "3 days");
	});

	test('bound', () {
		expect(1.0.bound(const Tuple2<double, double>(1, 3)), 1);
		expect(1.0.bound(const Tuple2<double, double>(0, 3)), 1);
		expect(1.0.bound(const Tuple2<double, double>(2, 3)), 2);
		expect(1.0.bound(const Tuple2<double, double>(1, 1)), 1);
		expect(1.0.bound(const Tuple2<double, double>(0, 0.5)), 0.5);

		expect(1.bound(const Tuple2<double, double>(1, 3)), 1);
		expect(1.bound(const Tuple2<double, double>(0, 3)), 1);
		expect(1.bound(const Tuple2<double, double>(2, 3)), 2);
		expect(1.bound(const Tuple2<double, double>(1, 1)), 1);
		expect(1.bound(const Tuple2<double, double>(-2, 0)), 0);

		expect(1.bound(const Tuple2<double, double>(1, 1)), 1);
		expect(() => 1.bound(const Tuple2<double, double>(2, 1)), throwsAssertionError);
	});

	test("Capitalization", () async {
		expect("abc".convertToTitleCase(), "Abc");
		expect("abc abc".convertToTitleCase(), "Abc Abc");
		expect("".convertToTitleCase(), "");
		expect("a".convertToTitleCase(), "A");
	});
}
