

import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:custom_tools/TimeAuthenticator.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

	setUp(() {
		// This has to be in setUp() not tearDown() for some reason
		testPrefs.clear();
	});

	tearDown(() {
		lastAuthenticationAttempt = null;
		expect(numErrorsToIgnoreForTesting, 0);
	});

	test('Initial call should fail', () async {
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), false);
	});

	test('Authentication time too far in future', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().add(const Duration(hours: 24)).millisecondsSinceEpoch;
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), false);
	});

	test('Authentication time too far in past', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().subtract(const Duration(hours: 24)).millisecondsSinceEpoch;
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), false);
	});

	test('Authentication time OK', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), true);
	});

	test('Pre authenticated should pass', () async {
		await authenticateTime();
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), true);
	});

	test('Blocked authentication attempt', () async {
		lastAuthenticationAttempt = DateTime.now();
		await authenticateTime();
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), false);
	});

	test('No longer blocked authentication attempt', () async {
		lastAuthenticationAttempt = DateTime.now().subtract(const Duration(minutes: 2));
		await authenticateTime();
		expect(timeIsValidAndTryReauthenticateTimeIfNot(), true);
	});

}
