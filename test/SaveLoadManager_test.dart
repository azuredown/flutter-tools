

import 'dart:io';
import 'dart:typed_data';

import 'package:binary_codec/binary_codec.dart';
import 'package:custom_tools/BasicFunctions.dart';
import 'package:custom_tools/SaveLoadManager.dart';
import 'package:custom_tools/SharedPrefsWrapper.dart';
import 'package:file/memory.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:path/path.dart';
import 'package:path_provider_windows/path_provider_windows.dart';
import 'package:tuple/tuple.dart';

void main() {

	setUp(() {
		if (Platform.isWindows) {
			PathProviderWindows.registerWith();
		}
	});

	tearDown(() {
		testPrefs.clear();
		expect(numErrorsToIgnoreForTesting, 0);
	});

	test('Save Twice Should Not Block Itself', () async {
		prefsSetInt("LastAuthenticatedTime", DateTime.now().millisecondsSinceEpoch);
		await saveFile(saveMap: false);
		await saveFile(saveMap: false);
	});

	test('After Save Flags Should Indicate Not Writing To Temp Directory And Only Be One Byte', () async {
		prefsSetInt("LastAuthenticatedTime", DateTime.now().millisecondsSinceEpoch);
		final MemoryFileSystem fileSystem = await saveFile(saveMap: false);
		final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
		int flagFilesFound = 0;
		for (int i = 0; i < files.length; i++) {
			if (files[i].path.endsWith("flag")) {
				flagFilesFound++;
				expect(await fileSystem.file(files[i].path).length(), 1);
				expect(await getFirstByte(fileSystem.file(files[i].path)), NOT_WRITING_TO_TEMP);
			}
		}
		expect(flagFilesFound, 2);
	});

	test('Both Saves Should Be Identical', () async {
		prefsSetInt("LastAuthenticatedTime", DateTime.now().millisecondsSinceEpoch);
		final MemoryFileSystem fileSystem = await saveFile(saveMap: false);
		final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
		int nonFlagFilesFound = 0;
		for (int i = 0; i < files.length; i++) {
			if (!files[i].path.endsWith("flag")) {
				nonFlagFilesFound++;
				expect(fileSystem.file(files[i].path).readAsStringSync(), "ABC");
			}
		}
		expect(nonFlagFilesFound, 4);
	});

	test('Dont Save To Authenticated Save If Incorrect Timestamp', () async {
		final MemoryFileSystem fileSystem = await saveFile(saveMap: false, numFilesToExpect: 3);
		await checkNonFlagFiles(fileSystem, 2, allowableSuffixes: <String>["2", "2temp"]);

		// Disabled, flaky test due to date time being off sometimes
		// We're in the future (over limit)
		//fileSystem = await saveFile(saveMap: false, injectedTimeForTesting: DateTime.now().subtract(const Duration(hours: 1)));
		//await checkNonFlagFiles(fileSystem, 4);
	});

	test('Loaded Data Is The Same As Saved Data', () async {
		prefsSetInt("LastAuthenticatedTime", DateTime.now().millisecondsSinceEpoch);
		final int now = DateTime.now().millisecondsSinceEpoch - 100000;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: now);
		final Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item2.length, 1);
		expect(data.item2[0], now);
		expect(data.item1! > 100000, true);
		expect(data.item1! < 150000, true);
	});

	test('Load From Authenticated Save Even If It Is In The Future', () async {
		final int time = DateTime.now().millisecondsSinceEpoch;
		prefsSetInt("LastAuthenticatedTime", time);
		final int futureTime = time + 1000000;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		numErrorsToIgnoreForTesting = 3;
		final Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item2.length, 1);
		expect(data.item2[0], futureTime);
		expect(data.item1! > -1005000, true);
		expect(data.item1! <= -905000, true);
	});

	test('Don\'t Load From Unauthenticated Saves In The Future', () async {
		// Does this by saving twice. The first time all files are saved. The second
		// only unauthenticated files allowing us to have authenticated and
		// unauthenticated files being different
		final int futureTime = DateTime.now().millisecondsSinceEpoch + 1000000;
		final int futureTime2 = futureTime + 1000000;
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		testPrefs.clear();
		await saveFile(saveMap: true, timeStampForMap: futureTime2, existingFileSystem: fileSystem);
		numErrorsToIgnoreForTesting = 3;
		final Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item2.length, 1);
		expect(data.item2[0], futureTime);
		expect(data.item1! > -1005000, true);
		expect(data.item1! <= -905000, true);
	});

	test('Test Backup Save Works And Is Identical (Authenticated Save, invalid time stamp)', () async {
		final int futureTime = DateTime.now().millisecondsSinceEpoch + 1000000;
		final int futureTime2 = futureTime + 1000000;
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		testPrefs.clear();
		await saveFile(saveMap: true, timeStampForMap: futureTime2, existingFileSystem: fileSystem);
		numErrorsToIgnoreForTesting = 3;
		await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		);
		expect((await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList()).length, 7);

		// Delete all non-backup files
		final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].path.endsWith("backup")) {
				fileSystem.file(files[i].path).deleteSync();
			}
		}
		expect((await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList()).length, 1);

		expect(numErrorsToIgnoreForTesting, 0);
		numErrorsToIgnoreForTesting = 2;
		final Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(numErrorsToIgnoreForTesting, 0);
		expect(data.item2.length, 1);
		expect(data.item2[0], futureTime);
		expect(data.item1! > -1050000, true);
		expect(data.item1! <= -950000, true);
	});

	test('Test Backup Save Works And Is Identical (Authenticated Save)', () async {
		final int futureTime = DateTime.now().millisecondsSinceEpoch + 1000000;
		final int futureTime2 = DateTime.now().millisecondsSinceEpoch;
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		testPrefs.clear();
		await saveFile(saveMap: true, timeStampForMap: futureTime2, existingFileSystem: fileSystem);
		await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		);
		expect((await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList()).length, 7);

		// Delete all non-backup files
		final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].path.endsWith("backup")) {
				fileSystem.file(files[i].path).deleteSync();
			}
		}
		expect((await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList()).length, 1);

		expect(numErrorsToIgnoreForTesting, 0);
		numErrorsToIgnoreForTesting = 1;
		final Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(numErrorsToIgnoreForTesting, 0);
		expect(data.item2.length, 1);
		expect(data.item2[0], futureTime2);
		expect(data.item1! >= 0, true);
		expect(data.item1! < 10000, true);
	});

	// Don't know how to test this and not that important so just ignore it
	test('Test Backup Save Is Only Triggered Once', () async {
	}, skip: "Not implemented yet");

	test('Ensure Flag Determines Which File To Load From (Unauthenticated)', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final int futureTime = DateTime.now().millisecondsSinceEpoch - 1000000;
		final int futureTime2 = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		File fileToWriteTo = fileSystem.file(join((await getDocumentsDirectoryWrapper()).path, "2"));
		await fileToWriteTo.writeAsBytes(binaryCodec.encode(<int, int>{ 0 : futureTime2 }), flush: true);

		Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item1! > 1000000, true);
		expect(data.item1! <= 1500000, true);

		fileToWriteTo = fileSystem.file(join((await getDocumentsDirectoryWrapper()).path, "2flag"));
		await fileToWriteTo.writeAsBytes(Uint8List.fromList(<int>[WRITING_TO_TEMP]), flush: true);

		data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item1! >= 0, true);
		expect(data.item1! < 10000, true);
	});

	test('Ensure Flag Determines Which File To Load From (Authenticated)', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final int futureTime = DateTime.now().millisecondsSinceEpoch - 1000000;
		final int futureTime2 = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: futureTime);
		File fileToWriteTo = fileSystem.file(join((await getDocumentsDirectoryWrapper()).path, "1"));
		await fileToWriteTo.writeAsBytes(binaryCodec.encode(<int, int>{ 0 : futureTime2 }), flush: true);

		// Delete all non-authenticated files
		final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
		for (int i = 0; i < files.length; i++) {
			if (files[i].path.endsWith("2") || files[i].path.endsWith("2temp")) {
				fileSystem.file(files[i].path).deleteSync();
			}
		}

		Tuple2<int?, Map<dynamic, dynamic>> data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item1! > 1000000, true);
		expect(data.item1! <= 1500000, true);

		fileToWriteTo = fileSystem.file(join((await getDocumentsDirectoryWrapper()).path, "1flag"));
		await fileToWriteTo.writeAsBytes(Uint8List.fromList(<int>[WRITING_TO_TEMP]), flush: true);

		data = (await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		))!;
		expect(data.item1! >= 0, true);
		expect(data.item1! < 10000, true);
	});

	test('Can delete file', () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = await saveFile(saveMap: true, timeStampForMap: DateTime.now().millisecondsSinceEpoch);

		await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		);

		// Expect that we have a backup file
		expect(await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().length, 7);

		await deleteFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			testFileSystem: fileSystem,
		);

		expect(await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().length, 0);

	});

	group("executeOperations()", () {
		test("executeOperations() simple", () async {
			for (int i = 0; i < 10; i++) {
				operationQueue.add(() async {
					expect(operationQueue.isEmpty, false);
					expect(locked, true);
				});
			}
			operationQueue.add(() async {
				expect(locked, true);
			});
			expect(operationQueue.isEmpty, false);
			await executeOperations();
			expect(operationQueue.isEmpty, true);
			expect(locked, false);
		});

		test("executeOperations() late additions", () async {
			for (int i = 0; i < 10; i++) {
				operationQueue.add(() async {
					expect(operationQueue.isEmpty, false);
					expect(locked, true);
				});
			}
			operationQueue.add(() async {
				expect(locked, true);
			});
			expect(operationQueue.isEmpty, false);
			final Future<void> operation1 = executeOperations();
			operationQueue.add(() async {
				expect(locked, true);
			});
			await operation1;
			expect(operationQueue.isEmpty, true);
			expect(locked, false);
		});

		test("executeOperations conflict", () async {
			for (int i = 0; i < 10; i++) {
				operationQueue.add(() async {
					expect(operationQueue.isEmpty, false);
					expect(locked, true);
				});
			}
			operationQueue.add(() async {
				expect(locked, true);
			});
			expect(operationQueue.isEmpty, false);
			final Future<void> operation1 = executeOperations();
			final Future<void> operation2 = executeOperations();
			expect(locked, true);
			await operation1;
			await operation2;
			expect(operationQueue.isEmpty, true);
			expect(locked, false);
		});
	});

	test("Save 2 files, should both save and in order", () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = MemoryFileSystem();
		await fileSystem.directory(await getDocumentsDirectoryWrapper()).create();

		final Future<void> save1 = saveToFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			dataToSave: binaryCodec.encode(<int, int>{1 : 1}),
			onSaveUnauthenticated: () {},
			tryDisplaySavedBlockedMessage: () {},
			testFileSystem: fileSystem,
		);

		final Future<void> save2 = saveToFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			dataToSave: binaryCodec.encode(<int, int>{2 : 2}),
			onSaveUnauthenticated: () {},
			tryDisplaySavedBlockedMessage: () {},
			testFileSystem: fileSystem,
		);

		await save1;
		await save2;

		expect((await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "1",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		)).toString(), "[null, {2: 2}]");

		expect((await readMostRecentValidSaveFile(
			authenticatedSaveName: "2",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		)).toString(), "[null, {2: 2}]");
	});

	test("Save and delete. Should do it in order", () async {
		testPrefs["LastAuthenticatedTime"] = DateTime.now().millisecondsSinceEpoch;
		final MemoryFileSystem fileSystem = MemoryFileSystem();
		await fileSystem.directory(await getDocumentsDirectoryWrapper()).create();

		final Future<void> save = saveToFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			dataToSave: binaryCodec.encode(<int, int>{1 : 1}),
			onSaveUnauthenticated: () {},
			tryDisplaySavedBlockedMessage: () {},
			testFileSystem: fileSystem,
		);

		final Future<void> delete = deleteFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "2",
			backupSaveName: "3",
			testFileSystem: fileSystem,
		);

		await save;
		await delete;

		expect((await readMostRecentValidSaveFile(
			authenticatedSaveName: "1",
			unauthenticatedSaveName: "1",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		)).toString(), "null");

		expect((await readMostRecentValidSaveFile(
			authenticatedSaveName: "2",
			unauthenticatedSaveName: "2",
			backupSaveName: "backup",
			saveTimeKey: 0,
			testFileSystem: fileSystem,
		)).toString(), "null");
	});

}

Future<MemoryFileSystem> saveFile({
	required bool saveMap,
	int numFilesToExpect = 6,
	int? timeStampForMap,
	MemoryFileSystem? existingFileSystem,
}) async {

	final MemoryFileSystem fileSystem = existingFileSystem ?? MemoryFileSystem(style: Platform.isWindows ? FileSystemStyle.windows : FileSystemStyle.posix);
	await fileSystem.directory(await getDocumentsDirectoryWrapper()).create(recursive: true);

	if (existingFileSystem == null) {
		expect((await fileSystem.directory(await getDocumentsDirectoryWrapper())
				.list()
				.toList()).length, 0);
	}

	await saveToFile(
		authenticatedSaveName: "1",
		unauthenticatedSaveName: "2",
		dataToSave: saveMap
			? binaryCodec.encode(<int, int>{ 0 : timeStampForMap ?? DateTime.now().millisecondsSinceEpoch })
			: Uint8List.fromList(<int>[65, 66, 67]),
		onSaveUnauthenticated: () {},
		tryDisplaySavedBlockedMessage: () {},
		testFileSystem: fileSystem,
	);

	final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();

	expect(files.length, numFilesToExpect, reason: files.toString());

	return fileSystem;
}

Future<void> checkNonFlagFiles(MemoryFileSystem fileSystem, int numExpected, {List<String>? allowableSuffixes}) async {
	int nonFlagFilesFound = 0;
	final List<FileSystemEntity> files = await fileSystem.directory(await getDocumentsDirectoryWrapper()).list().toList();
	for (int fileIndex = 0; fileIndex < files.length; fileIndex++) {
		if (!files[fileIndex].path.endsWith("flag")) {
			nonFlagFilesFound++;
			if (allowableSuffixes != null) {
				for (int suffixIndex = 0; suffixIndex < allowableSuffixes.length; suffixIndex++) {
					if (files[fileIndex].path.endsWith(allowableSuffixes[suffixIndex])) {
						break;
					}
					if (suffixIndex == allowableSuffixes.length - 1) {
						fail("Failed to find file ${files[fileIndex].path} in suffixes: $allowableSuffixes");
					}
				}
			}
		}
	}
	expect(numExpected, nonFlagFilesFound);
}
